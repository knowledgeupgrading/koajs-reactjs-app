import axios from "axios";

import {conf} from "../../conf";

const instance = axios.create({
    baseURL: `${conf.api.url}/v${conf.api.version}/`,
    transformRequest: [
        function (data, /*headers*/) {
            // headers["Accept-Language"] = `${localStorage.getItem("language")}`;
            return JSON.stringify(data);
        }
    ],
    headers: {
        "Content-Type": "application/json"
    }
});

export default instance;