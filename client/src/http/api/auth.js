import http from "../instances";

export const login = data => http.v1.post(`auth/login`, data);
export const refreshToken = data => http.v1.post(`auth/refreshToken`, data);
export const logout = data => http.v1.post(`auth/logout`, data);

