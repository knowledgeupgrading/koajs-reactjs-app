import qs from "qs";

export const stringifyQueryString = queryString => {
    return qs.stringify(queryString, {
        arrayFormat: "repeat",
        skipNulls: true,
    });
};