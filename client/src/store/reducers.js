import { combineReducers } from "redux";

import authReducer from '../domains/Auth/store/reducers';

const rootReducer = combineReducers({
    auth: authReducer,
});

export default rootReducer;