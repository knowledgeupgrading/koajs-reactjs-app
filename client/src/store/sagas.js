import { all } from "redux-saga/effects";

import {authWatchers} from '../domains/Auth/store/sagas';

export default function* rootSaga() {
    yield all([
        authWatchers(),
    ]);
}