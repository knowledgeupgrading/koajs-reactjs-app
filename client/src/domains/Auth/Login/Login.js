import React, {Component, Fragment} from 'react';

import { Button } from "../../../components/Button";

class Login extends Component {
    render() {
        return (
            <Fragment>
                <Button title={"Login with Facebook"} type={"facebook"} />
                <Button title={"Login with Google"} type={"google"} />
            </Fragment>
        );
    }
}

export default Login;