import * as actionTypes from "./actionTypes";

export const loginInit = (email, password) => {
    return {
        type: actionTypes.LOGIN_INIT,
        email,
        password
    };
};

export const loginStart = () => {
    return {
        type: actionTypes.LOGIN_START
    };
};

export const loginSuccess = user => {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        user
    };
};

export const loginFailure = error => {
    return {
        type: actionTypes.LOGIN_FAILURE,
        error
    };
};