import { put, call, takeLatest } from "redux-saga/effects";

import * as actionTypes from "./actionTypes";
import * as actions from "./actions";
import api from "../../../http/api";
import { handleApiErrors } from "../../../utils";

export function* authWatchers() {
    yield takeLatest(actionTypes.LOGIN_INIT, login);
}

function* login(action) {
    try {
        yield put(actions.loginStart());

        const { email, password } = action;
        const response = yield call(api.login, {
            email,
            password
        });
        const responseData = response.data;

        yield put(
            actions.loginSuccess(responseData)
        );
    } catch (error) {
        yield put(
            actions.loginFailure(handleApiErrors(error))
        );
    }
}