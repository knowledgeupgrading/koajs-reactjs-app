export const LOGIN_INIT = "auth/LOGIN_INIT";
export const LOGIN_START = "auth/LOGIN_START";
export const LOGIN_SUCCESS = "auth/LOGIN_SUCCESS";
export const LOGIN_FAILURE = "auth/LOGIN_FAILURE";