import * as actionTypes from "./actionTypes";
import { updateObject } from "../../../utils";

const initialState = {
    user: null,
    loading: false,
    error: null,
};

const loginStart = state => {
    return updateObject(state, {
        loading: true,
    });
};

const loginSuccess = (state, action) => {
    return updateObject(state, {
        loading: false,
        user: action.user
    });
};

const loginFailure = (state, action) => {
    return updateObject(state, {
        loading: false,
        error: action.error
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {

        case actionTypes.LOGIN_START:
            return loginStart(state);
        case actionTypes.LOGIN_SUCCESS:
            return loginSuccess(state, action);
        case actionTypes.LOGIN_FAILURE:
            return loginFailure(state, action);

        default:
            return state;
    }
};

export default reducer;
