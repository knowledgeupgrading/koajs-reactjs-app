export const conf = {
    app: {
        url: process.env.REACT_APP_URL
    },
    api: {
        url: process.env.REACT_APP_API_URL,
        version: process.env.REACT_APP_API_VERSION,
    },
};

export default conf;
