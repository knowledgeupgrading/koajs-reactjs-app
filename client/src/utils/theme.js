import iconFacebook from '../assets/icon_facebook.png';
import iconGoogle from '../assets/icon_google.png';

export const theme = {
    name: "Koa React App",
    iconFacebook,
    iconGoogle
};