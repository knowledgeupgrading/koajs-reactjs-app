export const handleApiErrors = error => {

    if (typeof error !== "undefined") {

        if (
            typeof error.response !== 'undefined' &&
            Object.prototype.hasOwnProperty.call(error, "response") &&
            Object.prototype.hasOwnProperty.call(error.response, "data")
        ) {
            if (Object.prototype.hasOwnProperty.call(error.response.data, "error")) {
                return error.response.data.error;
            } else if (Object.prototype.hasOwnProperty.call(error.response.data, 'errors')) {
                return {
                    message: error.response.data.errors,
                    status: error.response.data.status
                };
            } else if (Object.prototype.hasOwnProperty.call(error.response.data, 'message')) { // 500
                return {
                    message: error.response.data.message,
                    status: error.response.status
                };
            }
        }
    }

    return {
        message: 'Something went wrong'
    };
};