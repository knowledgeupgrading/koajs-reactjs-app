import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`

    html, body {
        height: 100%;
    }
    
    body {
        padding: 50px;
    }
`;
