import React from 'react';

import {StyledButton} from './Button.styled';

export const Button = ({title, type}) => (
    <StyledButton type={type}>
        {title}
    </StyledButton>
);