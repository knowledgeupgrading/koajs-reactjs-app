import styled from 'styled-components';

export const StyledButton = styled.button`
  box-sizing: border-box;
  position: relative;
  margin: 0.2em;
  padding: 0 15px 0 46px;
  border: none;
  text-align: left;
  line-height: 34px;
  white-space: nowrap;
  border-radius: 0.2em;
  font-size: 16px;
  color: #fff;
  ${props => props.type === 'google' ? 'text-shadow: 0 -1px 0 #354C8C;' : ''}
  ${props => props.type === 'google' ? 'background: #DD4B39;' : ''}
  ${props => props.type === 'facebook' ? 'background-color: #4C69BA;' : ''}
  ${props => props.type === 'facebook' ? 'background-image: linear-gradient(#4C69BA, #3B55A0);' : ''}
  
  &:before {
      content: "";
      box-sizing: border-box;
      position: absolute;
      top: 0;
      left: 0;
      width: 34px;
      height: 100%;
      border-right: ${props => props.type === 'facebook' ? '#364e92 1px solid' : '#BB3F30 1px solid'};
      background: url(${props => props.type === 'facebook' ? props.theme.iconFacebook : props.theme.iconGoogle}) 6px 6px no-repeat;
  }
  
  &:focus {
    outline: none;
    background-color: ${props => props.type === 'facebook' ? '#5B7BD5' : '#E74B37'};
    ${props => props.type === 'facebook' ? 'background-image: linear-gradient(#5B7BD5, #4864B1);' : ''};
  }
  
  &:hover {
    background-color: ${props => props.type === 'facebook' ? '#5B7BD5' : '#E74B37'};
    ${props => props.type === 'facebook' ? 'background-image: linear-gradient(#5B7BD5, #4864B1);' : ''};
  }
  
  &:active {
    box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
  }
`;