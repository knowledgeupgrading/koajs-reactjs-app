import React, {Fragment} from "react";

import { GlobalStyles } from "./GlobalStyles";

export const Layout = ({children}) => (
    <Fragment>
        <GlobalStyles />
        {children}
    </Fragment>
);
