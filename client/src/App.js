import React, {lazy, Suspense} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';

import {Layout, PrivateRoute} from './components';


const lazyImport = path => lazy(() => import(`${path}`));

const App = () => {
  return (
      <Layout>
          <Suspense fallback={"Loading..."}>
              <Switch>
                  <Route
                      exact
                      path="/"
                      component={lazyImport('./domains/Home/Home')}
                  />
                  <PrivateRoute
                      isAuthenticated
                      exact
                      path="/profile"
                      component={lazyImport(
                          "./domains/Profile/Profile"
                      )}
                  />
                  <Redirect to="/" />
              </Switch>
          </Suspense>
      </Layout>
  );
};

export default App;
