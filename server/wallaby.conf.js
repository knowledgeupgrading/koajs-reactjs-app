const packageJson = require('./package');
const config = require('./app/config');

const { files: tests } = packageJson.ava;

const envString = Object.entries(config)
    .map(([key, value]) => `${key}=${value}`)
    .join(';');

module.exports = () => ({
    files: [
        'app/**/*.js',
        'data/**/*.*',
    ],
    tests,
    env: {
        type: 'node',
        params: {
            env: envString,
        },
    },
    debug: true,
    testFramework: 'ava',
});