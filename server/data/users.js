const { hashSync } = require('bcryptjs');

module.exports = [
    {
        id: 1,
        email: 'test@test.loc',
        password: hashSync('1'),
    },
    {
        id: 2,
        email: 'test2@test.loc',
        password: hashSync('2'),
    },
];

