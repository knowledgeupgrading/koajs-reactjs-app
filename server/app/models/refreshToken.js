const Sequelize = require('sequelize');

const dbConnect = require('../utils/database/mysql');

class RefreshToken extends Sequelize.Model {}
RefreshToken.init({
  userId: Sequelize.INTEGER,
  token: Sequelize.STRING
}, { sequelize: dbConnect, modelName: 'refresh_token' });

module.exports = RefreshToken;
