const Sequelize = require('sequelize');

const dbConnect = require('../utils/database/mysql');

class User extends Sequelize.Model {}
User.init({
  firstName: Sequelize.STRING,
  lastName: Sequelize.STRING,
  email: {
    type: Sequelize.STRING,
    unique: true
  },
  password: Sequelize.STRING
}, { sequelize: dbConnect, modelName: 'user' });

module.exports = User;
