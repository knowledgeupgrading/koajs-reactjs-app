require('dotenv').config();

module.exports = {
    app: {
        port: process.env.APP_PORT
    },
    jwt: {
        secret: process.env.JWT_SECRET,
    },
    database: {
        mysql: {
            host: process.env.MYSQL_HOST,
            username: process.env.MYSQL_USER,
            password: process.env.MYSQL_PASSWORD,
            database: process.env.MYSQL_DB,
            dialect: 'mysql'
        }
    }
};