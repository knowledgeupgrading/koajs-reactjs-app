const { resolve } = require('path');

// eslint-disable-next-line import/no-dynamic-require
const users = require(resolve(
    __dirname,
    '..',
    '..',
    'data',
    'users'
));
const User = require('../models/user');

const findOne = query => {
    return User.findOne({
        where: query
    });
};

async function list() {
    return users;
}

async function update(query, values) {
    const entry = await findOne(query);
    if (entry) {
        Object.assign(entry, values);
    }
}

module.exports = {
    findOne,
    list,
    update,
};