const { compareSync } = require('bcryptjs');
const uuid = require('uuid/v4');
const jwt = require('jsonwebtoken');

const userService = require('../services/user');
const refreshTokenService = require('../services/refreshToken');
const config = require('../config');

const issueTokenPair = async (userId) => {
    const newRefreshToken = uuid();

    await refreshTokenService.add({
        token: newRefreshToken,
        userId,
    });

    // eslint-disable-next-line no-console
    console.log('Refresh Token has been stored.', refreshTokenService.findOne({token: newRefreshToken}));

    return {
        token: jwt.sign({ id: userId }, config.jwt.secret),
        refreshToken: newRefreshToken,
    };
};

const login = async ctx => {
    const { email, password } = ctx.request.body;
    const user = await userService.findOne({ email });

    if (!user || !compareSync(password, user.password)) {
        const error = new Error();
        error.status = 403;
        throw error;
    }
    ctx.body = await issueTokenPair(user.id);
};

const getRefreshToken = async ctx => {
    const { refreshToken } = ctx.request.body;
    const dbToken = await refreshTokenService.findOne({ token: refreshToken });

    if (!dbToken) {
        return;
    }

    await refreshTokenService.remove({
        token: refreshToken,
    });

    ctx.body = await issueTokenPair(dbToken.userId);
};

const logout = async ctx => {
    const { id: userId } = ctx.state.user;
    await refreshTokenService.remove({
        userId,
    });
    ctx.body = { success: true };
};

module.exports = {
    login,
    getRefreshToken,
    logout
};