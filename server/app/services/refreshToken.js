const { resolve } = require('path');

// eslint-disable-next-line import/no-dynamic-require
let tokens = require(resolve(
    __dirname,
    '..',
    '..',
    'data',
    'refreshTokens'
));
const RefreshToken = require('../models/refreshToken');

const findOne = async query => {
    return RefreshToken.findOne({
        where: query
    });
};

const add = async query => {
    const token = await RefreshToken.create(query);
    if (!token) {
        const error = new Error('The refresh token has not added');
        error.status = 500;
        throw error;
    }

    return token;
};

async function remove(query) {
    const token = await RefreshToken.destroy({
        where: query
    });
    if (!token) {
        const error = new Error('The refresh token has not removed');
        error.status = 500;
        throw error;
    }

    return token;
}

module.exports = {
    findOne,
    add,
    remove,
};