const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const jwtMiddleware = require('koa-jwt');

const config = require('./config');
const authRouter = require('./routes/auth');

const app = new Koa();
const router = new Router();

router.use('/auth', authRouter.routes());
router.get('/', ctx => {
    ctx.body = {message: "App index page"};
});

router.use(
    jwtMiddleware({
        secret: config.jwt.secret,
    })
);
app.use(bodyParser());
app.use(router.routes());
app.use(router.allowedMethods({
    throw: true,
}));

if (!module.parent) {
    app.listen(config.app.port, () => {
        console.log(`☀️ Server running on https://localhost:${config.app.port}`);
    });

}

module.exports = app;