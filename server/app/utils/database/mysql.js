const Sequelize = require('sequelize');
const config = require('../../config');

const sequelize = new Sequelize(
    config.database.mysql.database,
    config.database.mysql.username,
    config.database.mysql.password, {
        host: config.database.mysql.host,
        dialect: 'mysql'
    });
sequelize
    .authenticate()
    .then(() => {
        console.log('☀️ Database connection has been established successfully.');
    })
    .catch(err => {
        console.error('🌩 Unable to connect to the database:', err);
    });

module.exports = sequelize;