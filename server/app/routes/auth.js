const Router = require('koa-router');
const jwtMiddleware = require('koa-jwt');

const authController = require('../controllers/auth');
const config = require('../config');

const router = new Router();

router.post('/login', authController.login);
router.post('/refreshToken', authController.refreshToken);
router.post('/logout', jwtMiddleware({ secret: config.jwt.secret }), authController.logout);

module.exports = router;