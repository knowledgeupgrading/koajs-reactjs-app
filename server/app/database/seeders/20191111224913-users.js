const { hashSync } = require("bcryptjs");

module.exports = {
    up: (queryInterface) => {
        return queryInterface.bulkInsert("users", [
            {
                firstName: "Ihor",
                lastName: "Byra",
                email: "ihorbyra@gmail.com",
                password: hashSync("1"),
                createdAt: new Date(),
                updatedAt: new Date()
            },
          {
            firstName: "Igor",
            lastName: "Byra",
            email: "igorbyra@gmail.com",
            password: hashSync("2"),
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            firstName: "Test",
            lastName: "Testio",
            email: "test@test.loc",
            password: hashSync("1"),
            createdAt: new Date(),
            updatedAt: new Date()
          },
        ], {});
    },

    down: (queryInterface) => {
        return queryInterface.bulkDelete("users", null, {});
    }
};
