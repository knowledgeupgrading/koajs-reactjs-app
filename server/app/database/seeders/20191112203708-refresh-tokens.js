module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert("refresh_tokens", [
      {
        userId: 1,
        token: 'REFRESH_TOKEN_1',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        userId: 2,
        token: 'REFRESH_TOKEN_TO_DELETE_ON_LOGOUT',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete("refresh_tokens", null, {});
  }
};