const authService = require('../services/auth');

const login = async ctx => {
    await authService.login(ctx);
};

const refreshToken = async ctx => {
    await authService.getRefreshToken(ctx);
};

const logout = async ctx => {
    await authService.logout(ctx);
};

module.exports = {
    login,
    refreshToken,
    logout
};