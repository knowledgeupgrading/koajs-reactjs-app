const gulp = require('gulp');
const fs = require('fs');

const conf = require('./app/config');

gulp.task('create-config', async () => {
    const dir = './dest';
    const jsonContent = await JSON.stringify(conf.database.mysql);

    if (!fs.existsSync(dir)){
        await fs.mkdirSync(dir);
        console.log(`Directory ${dir} has been created.`);
    }

    fs.writeFile(`./${dir}/config.json`, jsonContent, 'utf8', (err) => {
        if (err) {
            console.log("An error occurred while writing JSON Object to File.");
            console.log(err);
        }
        console.log("JSON file has been saved.");
    });
});

gulp.task('default', gulp.series(['create-config']));