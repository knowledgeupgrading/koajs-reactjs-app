const test = require('ava');
const agent = require('supertest-koa-agent');

const app = require('../app');

const appAgent = agent(app);

test('User can successfully login', async t => {
    const res = await appAgent.post('/auth/login').send({
        email: 'test@test.loc',
        password: '1',
    });
    t.is(res.status, 200);
    t.truthy(typeof res.body.token === 'string');
    t.truthy(typeof res.body.refreshToken === 'string');
});

test('User gets 403 on invalid credentials', async t => {
    const res = await appAgent.post('/auth/login').send({
        email: 'INVALID@test.loc',
        password: 'INVALID',
    });
    t.is(res.status, 403);
});

test.todo('User receives 401 on expired token');

test('User can get new access token using refresh token', async t => {
    const res = await appAgent.post('/auth/refreshToken').send({
        refreshToken: 'REFRESH_TOKEN_1',
    });
    t.is(res.status, 200);
    t.truthy(typeof res.body.token === 'string');
    t.truthy(typeof res.body.refreshToken === 'string');
});

test('User get 404 on invalid refresh token', async t => {
    const res = await appAgent.post('/auth/refreshToken').send({
        refreshToken: 'INVALID_REFRESH_TOKEN',
    });
    t.is(res.status, 404);
});

test.todo('User can use refresh token only once');
test.todo('Refresh tokens become invalid on logout');
test.todo('Multiple refresh tokens are valid');