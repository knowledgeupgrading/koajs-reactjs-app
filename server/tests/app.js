const test = require('ava');
const agent = require('supertest-koa-agent');

const app = require('../app');

const appAgent = agent(app);

test('Index GET router works', async t => {
    const res = await appAgent.get('/');
    t.is(res.status, 200);
    t.is(res.type, 'application/json');
});